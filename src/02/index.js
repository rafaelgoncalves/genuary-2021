// Reproduction of Schotter by Georg Nees
// inspired in http://www.artsnova.com/Nees_Schotter_Tutorial.html

const sk01 = (p) => {
  const color = Math.floor(Math.random()*255);
  const rows = 22;
  const sqrSize = 5;
  const rnd = 0.22;
  const padding = 2*sqrSize;

  p.setup = function () {
    p.createCanvas((columns+4)*sqrSize, (rows+4)*sqrSize);
    p.background(255);
    p.stroke(0);
    p.smooth();
    p.noFill();
    p.rectMode(p.CENTER);
    p.noLoop();
  }

  p.draw = function () {
    for (let y=1; y <= rows; y++) {
      rndSum += (y*rndStep);
      for (let x=1; x <= columns; x++) {
        p.push();
        rndVal = -rndSum + 2*rndSum*Math.random();
        p.translate(
          padding + (x * sqrSize) - (0.5*sqrSize) + (rndVal * dampen),
          padding + (y * sqrSize) - (0.5*sqrSize) + (rndVal * dampen)
        );
        p.rotate(p.radians(rndVal));
        p.rect(0,0,sqrSize,sqrSize);
        p.pop();
      }
    }
  }
};
