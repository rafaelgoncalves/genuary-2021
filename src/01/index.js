// Rotating spheres
// inspired in https://editor.p5js.org/chandni/sketches/oDkj07rTF

const s01 = (p) => {
  const rotSpeed = 0.05;
  const rndSpeed = 0.02;
  const windowWidth = 500;
  const windowHeight = 500;
  const sqrSize = 400;
  const sphSize = 8;
  const inc = 60;
  const rand = 0.8;

  p.setup = function () {
    p.createCanvas(windowWidth, windowHeight, p.WEBGL);
  };

  p.draw = function () {
    p.background(255);
    p.noStroke();

    p.rotateX(p.frameCount * rotSpeed);
    p.rotateY(p.frameCount * rotSpeed);
    p.rotateZ(p.frameCount * rotSpeed);

    let factor = p.abs((1 - rand) + rand*p.sin(rndSpeed * p.frameCount));

    for (let x = -sqrSize / 2; x < sqrSize / 2; x += inc) {
      p.push();
      p.translate(x, 0, 0);
      for (let y = -sqrSize / 2; y < sqrSize / 2; y += inc) {
        p.push();
        p.translate(0, y, 0);
        for (let z = -sqrSize / 2; z < sqrSize / 2; z += inc) {
          p.push();
          p.translate(0, 0, z);
          p.sphere(sphSize*((x + y + z) * factor), 12, 12);
          p.pop();
        }
        p.pop();
      }
      p.pop();
    }
  };
};

const sk01 = {
  title: "Spheres",
  description: "",
  canvas: s01,
};
